package pl.edu.agh.ztis.filter;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import pl.edu.agh.ztis.model.RSSEntry;

/**
 * Created by Radek Dyrda on 4/18/2016.
 */
@Component
public class MongoExistFilter {

    @Autowired
    private MongoTemplate mongoTemplate;

    public boolean isEntryExist(Exchange exchange) {
        RSSEntry entry = exchange.getIn().getBody(RSSEntry.class);
        Query query = new Query();
        query.addCriteria(Criteria.where("url").is(entry.getUrl()));
        return mongoTemplate.exists(query, RSSEntry.class, entry.getRouteName());
    }
}
