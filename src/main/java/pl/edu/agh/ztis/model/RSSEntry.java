package pl.edu.agh.ztis.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */

@Document(collection = "RSSEntry")
public class RSSEntry implements Serializable {

    @Id
    private ObjectId id;

    private String title;

    private String content;

    private String url;

    private Long pubDate;

    private String routeName;

    private Integer contentWordsCount;

    private Integer titleWordsCount;

    private Integer contentSentencesCount;

    public RSSEntry(String title, String content, String url, Long pubDate, String routeName) {
        this.title = title;
        this.content = content;
        this.url = url;
        this.pubDate = pubDate;
        this.routeName = routeName;
    }

    public RSSEntry() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPubDate() {
        return pubDate;
    }

    public void setPubDate(Long pubDate) {
        this.pubDate = pubDate;
    }

    public Integer getContentWordsCount() {
        return contentWordsCount;
    }

    public void setContentWordsCount(Integer contentWordsCount) {
        this.contentWordsCount = contentWordsCount;
    }

    public Integer getTitleWordsCount() {
        return titleWordsCount;
    }

    public void setTitleWordsCount(Integer titleWordsCount) {
        this.titleWordsCount = titleWordsCount;
    }

    public Integer getContentSentencesCount() {
        return contentSentencesCount;
    }

    public void setContentSentencesCount(Integer contentSentencesCount) {
        this.contentSentencesCount = contentSentencesCount;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
