package pl.edu.agh.ztis.model.converter;

import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * Created by Radek Dyrda on 4/18/2016.
 */
public class MongoLongToDateConverter implements Converter<Long, Date> {
    @Override
    public Date convert(Long aLong) {
        return new Date(aLong);
    }
}
