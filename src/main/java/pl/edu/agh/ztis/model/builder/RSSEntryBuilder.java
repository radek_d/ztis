package pl.edu.agh.ztis.model.builder;

import pl.edu.agh.ztis.model.RSSEntry;

public class RSSEntryBuilder {
    private String title;
    private String content;
    private String url;
    private Long pubDate;
    private String routeName;

    public RSSEntryBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public RSSEntryBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public RSSEntryBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public RSSEntryBuilder setPubDate(Long pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public RSSEntryBuilder setRouteName(String routeName) {
        this.routeName = routeName;
        return this;
    }

    public RSSEntry createRSSEntry() {
        return new RSSEntry(title, content, url, pubDate, routeName);
    }

    public RSSEntry createRSSEntry(RSSEntry entry) {
        return new RSSEntry(entry.getTitle(), entry.getContent(), entry.getUrl(), entry.getPubDate(), entry.getRouteName());
    }
}