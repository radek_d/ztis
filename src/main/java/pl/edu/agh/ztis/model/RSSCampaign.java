package pl.edu.agh.ztis.model;

import org.springframework.data.mongodb.core.mapping.Document;
import pl.edu.agh.ztis.route.configuration.RSSRouteConfiguration;

import java.io.Serializable;

/**
 * Created by Radek Dyrda on 4/17/2016.
 */

@Document(collection = "twitterCampaigns")
public class RSSCampaign implements Serializable {

    private String name;

    private String status;

    private RSSRouteConfiguration configuration;

    public RSSCampaign() {
    }

    public RSSCampaign(String name, RSSRouteConfiguration configuration) {
        this.name = name;
        this.configuration = configuration;
        this.status = "started";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RSSRouteConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(RSSRouteConfiguration configuration) {
        this.configuration = configuration;
    }
}
