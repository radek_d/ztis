package pl.edu.agh.ztis.web.rest.command;

/**
 * Created by Radek Dyrda on 4/17/2016.
 */
public class RSSCampaignConfigurationCommand {

    private String campaignName;

    private String url;

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
