package pl.edu.agh.ztis.web.rest.view;

import pl.edu.agh.ztis.model.RSSCampaign;

import java.io.Serializable;

/**
 * Created by Radek Dyrda on 4/18/2016.
 */
public class RSSCampaignJSONView implements Serializable {

    private RSSCampaign rssCampaign;

    private Long feedCount;

    private String lastFeedDateStr;

    private RSSCampaignJSONView(Builder builder) {
        this.rssCampaign = builder.rssCampaign;
        this.feedCount = builder.feedCount;
        this.lastFeedDateStr = builder.lastFeedDateStr;
    }

    public RSSCampaignJSONView() {
    }

    public RSSCampaign getRssCampaign() {
        return rssCampaign;
    }

    public void setRssCampaign(RSSCampaign rssCampaign) {
        this.rssCampaign = rssCampaign;
    }

    public Long getFeedCount() {
        return feedCount;
    }

    public void setFeedCount(Long feedCount) {
        this.feedCount = feedCount;
    }

    public String getLastFeedDateStr() {
        return lastFeedDateStr;
    }

    public void setLastFeedDateStr(String lastFeedDateStr) {
        this.lastFeedDateStr = lastFeedDateStr;
    }

    public static class Builder {

        private RSSCampaign rssCampaign;
        private Long feedCount;
        private String lastFeedDateStr;

        public Builder setRssCampaign(RSSCampaign rssCampaign) {
            this.rssCampaign = rssCampaign;
            return this;
        }

        public Builder setFeedCount(Long feedCount) {
            this.feedCount = feedCount;
            return this;
        }

        public Builder setLastFeedDateStr(String lastFeedDateStr) {
            this.lastFeedDateStr = lastFeedDateStr;
            return this;
        }

        public RSSCampaignJSONView createRSSCampaignJSONView() {
            return new RSSCampaignJSONView(this);
        }
    }

}
