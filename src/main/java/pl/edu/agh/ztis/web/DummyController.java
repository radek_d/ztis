package pl.edu.agh.ztis.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.ztis.route.CamelRoutesManager;
import pl.edu.agh.ztis.route.RSSRouteManager;
import pl.edu.agh.ztis.route.configuration.RSSRouteConfiguration;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * Dummy controller for test purposes only
 */

@Controller
@RequestMapping("/")
public class DummyController {

    @Resource(name = "basicRoutesManager")
    private CamelRoutesManager manager;

    @Resource(name = "rssRoutesManager")
    private RSSRouteManager rssRouteManager;

    @RequestMapping(value = "log", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        String from = "rss://http://rss.wprost.pl/wprost-swiat?consumer.delay=1000";
        String to = "bean:toLogProcessor";

        manager.addRoute(from, Arrays.asList(to), "test");

        model.addAttribute("message", "Hello world!");
        return "hello";
    }

    @RequestMapping(value = "db", method = RequestMethod.GET)
    public String printDb(ModelMap model) {

        rssRouteManager.addRSSRoute(new RSSRouteConfiguration("http://rss.wprost.pl/wprost-swiat", "wprostswiat", "wprostswiat", "simpleAnalyser"));

        model.addAttribute("message", "Hello world!");
        return "hello";
    }
}