package pl.edu.agh.ztis.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ztis.model.RSSEntry;
import pl.edu.agh.ztis.model.converter.MongoLongToDateConverter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Radek Dyrda on 4/18/2016.
 */
@RestController
@RequestMapping("/RSSEntry")
public class RSSEntryRestController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        Set<Converter> converters = new HashSet<>();
        converters.add(new MongoLongToDateConverter());
        ConversionServiceFactoryBean factoryBean = new ConversionServiceFactoryBean();
        factoryBean.setConverters(converters);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/campaign/{campaignName}")
    Collection<RSSEntry> getFromCampaign(@PathVariable String campaignName) {
        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("routeName").is(campaignName));
        return mongoTemplate.find(findQuery, RSSEntry.class, campaignName);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/campaign/{campaignName}/{entryId}")
    RSSEntry getTweet(@PathVariable String campaignName, @PathVariable String entryId) {
        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("_id").is(entryId));
        return mongoTemplate.findOne(findQuery, RSSEntry.class, campaignName);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/campaign/{campaignName}/{entryId}")
    void deleteTweet(@PathVariable String campaignName, @PathVariable String entryId) {
        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("_id").is(entryId));
        mongoTemplate.remove(findQuery, RSSEntry.class, campaignName);
    }
}
