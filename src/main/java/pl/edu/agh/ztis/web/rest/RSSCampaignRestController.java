package pl.edu.agh.ztis.web.rest;

import org.apache.camel.CamelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ztis.model.RSSCampaign;
import pl.edu.agh.ztis.model.RSSEntry;
import pl.edu.agh.ztis.route.RSSRouteManager;
import pl.edu.agh.ztis.route.configuration.RSSRouteConfiguration;
import pl.edu.agh.ztis.web.rest.command.RSSCampaignConfigurationCommand;
import pl.edu.agh.ztis.web.rest.view.RSSCampaignJSONView;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radek Dyrda on 4/17/2016.
 */
@RestController
@RequestMapping("/RSSCampaign")
public class RSSCampaignRestController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Resource(name = "rssRoutesManager")
    private RSSRouteManager rssRouteManager;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Collection<RSSCampaignJSONView> getAll() {
        List<RSSCampaign> all = mongoTemplate.findAll(RSSCampaign.class);

        Query lastquery = new Query();
        lastquery.with(new Sort(Sort.Direction.DESC, "_id"));
        lastquery.limit(1);

        Collection<RSSCampaignJSONView> ret = all.stream().map(rssCampaign ->
                        new RSSCampaignJSONView.Builder()
                                .setRssCampaign(rssCampaign)
                                .setFeedCount(mongoTemplate.count(new Query(), rssCampaign.getConfiguration().getCollectionName()))
                                .setLastFeedDateStr(new Date(mongoTemplate.find(lastquery, RSSEntry.class, rssCampaign.getConfiguration().getCollectionName()).stream().findFirst().get().getPubDate()).toString())
                                .createRSSCampaignJSONView()
        ).collect(Collectors.toList());
        return ret;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{campaignName}", produces = "application/json")
    @ResponseBody
    public RSSCampaign get(@PathVariable String campaignName) {
        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(campaignName));

        return mongoTemplate.findOne(findQuery, RSSCampaign.class);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = "application/json")
    public ResponseEntity<?> addCampaign(RSSCampaignConfigurationCommand configuration) {
        if (StringUtils.isEmpty(configuration.getCampaignName())) {
            return new ResponseEntity<>("Campaign name not provided", HttpStatus.BAD_REQUEST);
        }
        if (StringUtils.isEmpty(configuration.getUrl())) {
            return new ResponseEntity<>("Pooling RSS url not provided", HttpStatus.BAD_REQUEST);
        }

        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(configuration.getCampaignName()));

        if (mongoTemplate.exists(findQuery, RSSCampaign.class)) {
            return new ResponseEntity<>("Campaign with given name already exists", HttpStatus.BAD_REQUEST);
        }

        RSSRouteConfiguration routeConfiguration = new RSSRouteConfiguration(
                configuration.getUrl(),
                configuration.getCampaignName(),
                configuration.getCampaignName(),
                "simpleAnalyser"
        );

        rssRouteManager.addRSSRoute(routeConfiguration);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{campaignName}/start", produces = "application/json")
    public ResponseEntity<?> startCampaign(@PathVariable String campaignName) {

        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(campaignName));

        if (!mongoTemplate.exists(findQuery, RSSCampaign.class)) {
            return new ResponseEntity<>("Campaign with given name already exists", HttpStatus.BAD_REQUEST);
        }

        try {
            rssRouteManager.startRoute(campaignName);
        } catch (CamelException e) {
            RSSRouteConfiguration configuration = mongoTemplate.findOne(findQuery, RSSCampaign.class).getConfiguration();
            rssRouteManager.addRSSRoute(configuration);
        }

        return ResponseEntity.ok().build();
    }

}
