package pl.edu.agh.ztis.processor;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import pl.edu.agh.ztis.route.DynamicRouteBuilder;

/**
 * Created by Radek Dyrda on 4/3/2016.
 */
public abstract class AbstractRSSProcessor implements Processor {

    public abstract Object doWithRSSEntry(SyndEntry feed, String routeName);

    @Override
    public void process(Exchange exchange) throws Exception {
        SyndFeed body = exchange.getIn().getBody(SyndFeed.class);

        final String routeName = exchange.getIn().getHeader(DynamicRouteBuilder.ROUTE_NAME_HEADER, String.class);

        exchange.getOut().setBody(body.getEntries().stream().map(entity -> doWithRSSEntry((SyndEntry) entity, routeName)).findFirst().get());
    }
}
