package pl.edu.agh.ztis.processor;

import com.sun.syndication.feed.synd.SyndEntry;
import org.springframework.stereotype.Component;
import pl.edu.agh.ztis.model.builder.RSSEntryBuilder;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */

@Component("rssToObjectProcessor")
public class RSSToObjectProcessor extends AbstractRSSProcessor {
    @Override
    public Object doWithRSSEntry(SyndEntry feed, String routeName) {
        return new RSSEntryBuilder() //
                .setTitle(feed.getTitle())//
                .setContent(null != feed.getDescription() ? feed.getDescription().getValue() : null) //
                .setUrl(feed.getLink()) //
                .setPubDate(feed.getPublishedDate().getTime()) //
                .setRouteName(routeName) //
                .createRSSEntry();
    }
}
