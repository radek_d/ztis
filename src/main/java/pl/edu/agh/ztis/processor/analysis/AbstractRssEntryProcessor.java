package pl.edu.agh.ztis.processor.analysis;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import pl.edu.agh.ztis.model.RSSEntry;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */
public abstract class AbstractRssEntryProcessor implements Processor {

    protected abstract RSSEntry doWithRssObject(RSSEntry input);

    @Override
    public void process(Exchange exchange) throws Exception {
        final RSSEntry outputRSSEntry = doWithRssObject(exchange.getIn().getBody(RSSEntry.class));

        exchange.getOut().setBody(outputRSSEntry);
    }
}
