package pl.edu.agh.ztis.processor;

import com.sun.syndication.feed.synd.SyndEntry;
import org.apache.camel.impl.DefaultMessage;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Created by Radek Dyrda on 4/3/2016.
 */

@Component("toLogProcessor")
public class RSSToLogProcessor extends AbstractRSSProcessor {

    private static final Logger log = Logger.getLogger(RSSToLogProcessor.class);

    @Override
    public Object doWithRSSEntry(SyndEntry feed, String routeName) {
        log.info("RSS: " + feed.getTitle() + ", date: " + feed.getPublishedDate());
        return new DefaultMessage();
    }

}
