package pl.edu.agh.ztis.processor.analysis;

import org.springframework.stereotype.Component;
import pl.edu.agh.ztis.model.RSSEntry;
import pl.edu.agh.ztis.model.builder.RSSEntryBuilder;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */

@Component("simpleAnalyser")
public class SimpleMetricsAnalyser extends AbstractRssEntryProcessor {
    @Override
    protected RSSEntry doWithRssObject(RSSEntry input) {
        RSSEntry out = new RSSEntryBuilder().createRSSEntry(input);
        out.setTitleWordsCount(countWords(input.getTitle()));
        out.setContentWordsCount(countWords(input.getContent()));
        out.setContentSentencesCount(countSentences(input.getContent()));
        return out;
    }

    private int countWords(String string) {
        if (null != string) {
            String[] splitted = string.split(" ");
            return splitted.length;
        }
        return 0;
    }

    private int countSentences(String string) {
        if (null != string) {
            String[] splitted = string.split("[\\.\\?!]");
            return splitted.length;
        }
        return 0;
    }
}
