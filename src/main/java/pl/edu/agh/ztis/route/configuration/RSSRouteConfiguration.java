package pl.edu.agh.ztis.route.configuration;

import org.springframework.stereotype.Component;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */
@Component("rssRouteManager")
public class RSSRouteConfiguration {

    private String analyserBean;

    private String routeName;

    private String collectionName;

    private String url;

    public RSSRouteConfiguration(String url, String collectionName, String routeName, String analyserBean) {
        this.url = url;
        this.collectionName = collectionName;
        this.routeName = routeName;
        this.analyserBean = analyserBean;
    }

    public RSSRouteConfiguration() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getAnalyserBean() {
        return analyserBean;
    }

    public void setAnalyserBean(String analyserBean) {
        this.analyserBean = analyserBean;
    }
}
