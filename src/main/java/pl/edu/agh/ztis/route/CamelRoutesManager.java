package pl.edu.agh.ztis.route;

import org.apache.camel.CamelException;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This component allows to add routes dynamically
 */

@Component("basicRoutesManager")
public class CamelRoutesManager {

    private static final Logger log = Logger.getLogger(CamelRoutesManager.class);

    @Autowired
    @Qualifier("camelContext")
    private SpringCamelContext camelContext;

    private Map<String, RouteDefinition> activeRoutes = new HashMap<>();

    public String addRoute(String from, List<String> to, String routeName) {
        DynamicRouteBuilder rtbuilder = new DynamicRouteBuilder(from, to, routeName);
        log.info("Adding new route " + from);
        try {
            camelContext.addRoutes(rtbuilder);

        } catch (Exception e) {
            log.warn("Route build failed", e);
            log.info("Avail routes:" + camelContext.getRouteDefinitions());
        }

        final String routeId = rtbuilder.getRouteDefinition().getId();
        activeRoutes.put(routeId, rtbuilder.getRouteDefinition());

        return routeId;
    }

    public Collection<String> getRoutes() {
        return activeRoutes.keySet();
    }

    public void deleteRoute(String routeId) throws CamelException {
        if (!getRoutes().contains(routeId)) {
            throw new CamelException(String.format("Route with %s not found.", routeId));
        } else {
            try {
                camelContext.stopRoute(routeId);
                camelContext.removeRoute(routeId);
                activeRoutes.remove(routeId);
            } catch (Exception e) {
                throw new CamelException(e);
            }

        }
    }

    public void stopRoute(String routeId) throws CamelException {
        if (!getRoutes().contains(routeId)) {
            throw new CamelException(String.format("Route with %s not found.", routeId));
        } else {
            try {
                camelContext.stopRoute(routeId);
            } catch (Exception e) {
                throw new CamelException(e);
            }

        }
    }

    public void startRoute(String routeId) throws CamelException {
        if (!getRoutes().contains(routeId)) {
            throw new CamelException(String.format("Route with %s not found.", routeId));
        } else {
            try {
                camelContext.startRoute(routeId);
            } catch (Exception e) {
                throw new CamelException(e);
            }

        }
    }

    public boolean isRouteExist(String routeId) {
        return getRoutes().contains(routeId);
    }

}
