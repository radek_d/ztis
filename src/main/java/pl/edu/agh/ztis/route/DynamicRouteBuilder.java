package pl.edu.agh.ztis.route;

import org.apache.camel.Expression;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.RouteDefinition;

import java.util.Arrays;
import java.util.List;

/**
 * Created by radek
 */
public class DynamicRouteBuilder extends RouteBuilder {

    public static final String ROUTE_NAME_HEADER = "routeName";
    private final List<String> to;
    private final String from;
    private final String routeName;
    private Expression filterBean = null;
    private RouteDefinition routeDefinition;

    public DynamicRouteBuilder(String from, List<String> to, String routeName) {
        this.to = to;
        this.from = from;
        this.routeName = routeName;
    }

    public DynamicRouteBuilder(List<String> to, String from, String routeName, Expression filterBean) {
        this.to = to;
        this.from = from;
        this.routeName = routeName;
        this.filterBean = filterBean;
    }

    public DynamicRouteBuilder(String from, String to, String routeName) {
        this(from, Arrays.asList(to), routeName);
    }

    @Override
    public void configure() throws Exception {
        String[] routeTo = to.stream().toArray(String[]::new);
        RouteDefinition rt = from(from);
        if (filterBean != null) {
            rt.filter().expression(filterBean);
        }
        routeDefinition = rt.setHeader(ROUTE_NAME_HEADER, constant(routeName)).to(routeTo);
    }

    public RouteDefinition getRouteDefinition() {
        return routeDefinition;
    }
}
