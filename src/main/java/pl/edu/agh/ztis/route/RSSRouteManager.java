package pl.edu.agh.ztis.route;

import org.apache.camel.CamelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import pl.edu.agh.ztis.model.RSSCampaign;
import pl.edu.agh.ztis.route.configuration.RSSRouteConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radek Dyrda on 4/10/2016.
 */
@Component("rssRoutesManager")
public class RSSRouteManager extends CamelRoutesManager {

    @Value("${mongodb.dbname}")
    private String dbName;

    @Autowired
    private MongoTemplate mongoTemplate;

    public String addRSSRoute(RSSRouteConfiguration configuration) {
        String dbEndpoint = "mongodb:mongoBean?database=" + dbName + "&collection=" + configuration.getCollectionName() + "&operation=insert";

        List<String> endpoints = new ArrayList<>();
        endpoints.add("bean:rssToObjectProcessor");
        if (null != configuration.getAnalyserBean()) {
            endpoints.add("bean:" + configuration.getAnalyserBean());
        }
        endpoints.add(dbEndpoint);

        String from = "rss://" + configuration.getUrl() + "?consumer.delay=1000";

        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(configuration.getRouteName()));

        if (!mongoTemplate.exists(query, RSSCampaign.class)) {
            mongoTemplate.insert(new RSSCampaign(configuration.getRouteName(), configuration));
        }
        return addRoute(from, endpoints, configuration.getRouteName());
    }

    @Override
    public void deleteRoute(String routeId) throws CamelException {
        super.deleteRoute(routeId);

        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(routeId));

        mongoTemplate.remove(findQuery, RSSCampaign.class);
    }

    @Override
    public void stopRoute(String routeId) throws CamelException {
        super.stopRoute(routeId);
        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(routeId));

        Update update = new Update();
        update.set("status", "stopped");

        mongoTemplate.updateFirst(findQuery, update, RSSCampaign.class);
    }

    @Override
    public void startRoute(String routeId) throws CamelException {
        super.startRoute(routeId);

        Query findQuery = new Query();
        findQuery.addCriteria(Criteria.where("name").is(routeId));

        Update update = new Update();
        update.set("status", "working");

        mongoTemplate.updateFirst(findQuery, update, RSSCampaign.class);
    }
}
