function fillCampaigns() {
    $.ajax({
        url: $('#serverUrl').val() + '/RSSCampaign/',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function () {
                appendCampaign(this.rssCampaign.name, this.rssCampaign.configuration.url, this.feedCount, this.lastFeedDateStr, this.rssCampaign.status);
            });
        }
    });
}

function appendCampaign(name, url, countCollected, lastCollected, status) {
    var htmlStr =
        '<div class="callout panel">' +
        '<div class="row">' +
        '   <div class="small-2 columns">' +
        '       <h5 class="right">Name: </h5>' +
        '   </div>' +
        '   <div class="small-5 columns">' +
        '       <h5>' + name + '</h5>' +
        '   </div>' +
        '   <div class="small-5 columns">' +
        '       <h5><a href="#" onclick="openmodal(\'' + name + '\');">Show entries</a></h5>' +
        '   </div>' +
        '   <div class="small-2 columns">' +
        '       <h5 class="right">url: </h5>' +
        '   </div>' +
        '   <div class="small-10 columns">' +
        '       <h5>' + url + '</h5>' +
        '   </div>' +
        '   <div class="small-2 columns">' +
        '       <h5 class="right">Collected: </h5>' +
        '   </div>' +
        '   <div class="small-10 columns">' +
        '       <h5>' + countCollected + '</h5>' +
        '   </div>' +
        '   <div class="small-2 columns">' +
        '       <h5 class="right">Last entity date: </h5>' +
        '   </div>' +
        '   <div class="small-10 columns">' +
        '       <h5>' + lastCollected + '</h5>' +
        '   </div>' +
        '   <div class="small-2 columns">' +
        '       <h5 class="right">Status: </h5>' +
        '   </div>' +
        '   <div class="small-10 columns">' +
        '       <h5>' + status + '</h5>' +
        '   </div>' +
        '</div>' +
        '</div>';
    $('#campaigns').append(htmlStr);
}

function openmodal(name) {
    $('#entriesModal').foundation('reveal', 'open');
    fillEntries(name);
}

function fillEntries(name) {
    $.ajax({
        url: $('#serverUrl').val() + '/RSSEntry/campaign/' + name,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#entryTable tr:not(#head)').remove();
            $.each(data, function () {
                appendEntry(this.title, this.content, this.url, this.pubDate);
            });
        }
    });
}

function appendEntry(title, desc, url, time) {
    var date = new Date(time);
    var str = '<tr>' +
        '<td>' + title + '</td>' +
        '<td>' + desc + '</td>' +
        '<td>' + date.toISOString() + '</td>' +
        '<td>' + url + '</td>' +
        '</tr>';
    $('#entryTable tr:last').after(str);
}